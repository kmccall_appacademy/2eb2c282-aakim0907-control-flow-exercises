# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  upcase_abc = ("A".."Z").to_a

  str.chars.select {|ch| upcase_abc.include?(ch)}.join("")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_idx = str.length / 2
  str.length.odd? ? str[mid_idx] : str[mid_idx-1..mid_idx]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = 0
  str.each_char {|ch| vowel_count += 1 if VOWELS.include?(ch)}
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  numbers = (1..num).to_a
  numbers.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined_str = ""
  arr.each_index do |idx|
    joined_str += arr[idx]
    next if idx == arr.length - 1
    joined_str += separator
  end
  joined_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdcase_converted = ""
  str.chars.each_with_index do |ele, idx|
    idx.odd? ? weirdcase_converted += ele.upcase : weirdcase_converted += ele.downcase
  end
  weirdcase_converted
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reversed = []
  str.split.each {|ele| ele.length >= 5 ? reversed << ele.reverse : reversed << ele}
  reversed.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz_arr = []
  (1..n).each do |ele|
    if ele % 15 == 0
      fizzbuzz_arr << "fizzbuzz"
    elsif ele % 3 == 0
      fizzbuzz_arr << "fizz"
    elsif ele % 5 == 0
      fizzbuzz_arr << "buzz"
    else
      fizzbuzz_arr << ele
    end
  end
  fizzbuzz_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2...num).each {|n| return false if num % n == 0}
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = []
  (1..num).each {|n| factors_arr << n if num % n == 0}
  factors_arr.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors_arr = factors(num)
  factors_arr.select {|ele| prime?(ele)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each {|ele| ele.even? ? evens << ele : odds << ele}
  evens.length == 1 ? evens.first : odds.first
end
